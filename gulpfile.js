'use strict';

var gulp = require('gulp'),
    git = require('gulp-git'),
    sass = require('gulp-sass'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    paths = {
        js: 'lib/js/*',
        sass: 'lib/sass/*'
    };

const dist = gulp.series(_distJs, _distSass);

module.exports = {
    default: dist,
    dist
};

function _distJs() {
    return gulp.src(paths.js)
        .pipe(concat('sp-table.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-table.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function _distSass() {
    return gulp.src(paths.sass)
        .pipe(sass())
        .pipe(concat('sp-table.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(minifyCss({keepSpecialComments: 0}))
        .pipe(rename('sp-table.min.css'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}