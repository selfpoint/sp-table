	(function(angular) {
	var name = 'spTable';

	function provider() {
		var self = this;

		self.templates = {
			actionsCheckboxes: {
				th: '<th><input type="checkbox" ng-model="tableCtrl.allActionsChecked" ng-change="tableCtrl.allActionsCheckedChanged()"/></th>',
				td: '<td><input type="checkbox" ng-model="item.actionsChecked" ng-change="tableCtrl.actionCheckboxChanged(item)"/></td>'
			}
		};

		self.$get = function() {
			return self;
		};
	}

	function directive(spTable) {
		/**
		 * Trim string
		 * @private
		 *
		 * @param {String/Null} str
		 */
		function _trim(str) {
			return (str || '').trim();
		}

		return {
			restrict: 'E',
			template: function(element, attrs) {
				var tHead = '',
					tBody = '';

				angular.forEach(element.children(), function(td) {
					if (td.tagName.toLowerCase() === 'sp-table-column') {
						var $td = angular.element(td),
							template = _trim($td.attr('template'));
						if (template && spTable.templates[template]) {
							tHead += spTable.templates[template].th;
							tBody += spTable.templates[template].td;
						} else {
							var	title = _trim($td.attr('title')),
								styleClass = _trim($td.attr('style-class')),
								sortBy = _trim($td.attr('sort-by')),
								sortByHtml = sortBy ? ' ng-click="tableCtrl.sort(\'' + sortBy.replace(/'/g, '\\\'') + '\')" aria-label="\''+ title  + '\' {{tableCtrl.label | translate}}" class="clickable ' + styleClass + '" ng-class="{sort: tableCtrl.sortBy == \'' + sortBy + '\', reverse: tableCtrl.sortBy == \'' + sortBy + '\' && tableCtrl.isReverse}"' : '';
							angular.forEach($td.children(), function(titleElement) {
								if (titleElement.tagName.toLowerCase() === 'sp-table-column-title') {
									var $titleElement = angular.element(titleElement);
									title = $titleElement.html();
									$titleElement.remove();
								}
							});

							tHead += '<th' + sortByHtml + (styleClass ? ' class="' + styleClass + '"' : '') + '><span>' + title + '</span></th>';
							tBody += '<td' + (styleClass ? ' class="' + styleClass + '"' : '') + '>' + $td.html() + '</td>';
						}
					}
				});

				return '' +
					'<table' + (attrs.styleClass ? ' class="' + attrs.styleClass +  '"' : '') + '>' +
					'	<thead>' +
					'		<tr>' + tHead + '</tr>' +
					'	</thead>' +
					'	<tbody ng-if="!tableCtrl.sortByCategories">' +
					'		<tr ' + (attrs.hasOwnProperty('rowInViewWatcher') ? 'sp-in-view-watcher="' + (attrs.rowInViewWatcher || '') + '" ' : '') +
					'			ng-repeat="item in tableCtrl.items | orderBy:tableCtrl.sortBy:tableCtrl.isReverse"' +
					'			' + (attrs.rowNgClick ? ' class="clickable" ng-click="' + attrs.rowNgClick + '"' : '') + '>' + tBody + '</tr>' +
					'	</tbody>' +
					'	<tbody ng-if="tableCtrl.sortByCategories" ng-repeat="category in tableCtrl.items">' +
					'  <th colspan="{{tableCtrl.colspan}}" class="category-header" scope="row">{{category[0].isPseudo ? category[0].product.categories && category[0].product.categories[0] ? (category[0].product.categories[0].names | name) : category[0].product.name : (category[0].product.family.categories[0].names | name)}}</th>' +
					'		<tr ' + (attrs.hasOwnProperty('rowInViewWatcher') ? 'sp-in-view-watcher="' + (attrs.rowInViewWatcher || '') + '" ' : '') +
					'			ng-repeat="item in category | orderBy:tableCtrl.sortBy:tableCtrl.isReverse"' +
					'			' + (attrs.rowNgClick ? ' class="clickable" ng-click="' + attrs.rowNgClick + '"' : '') + '>' + tBody + '</tr>' +
					'	</tbody>' +
					'</table>';
			},
			scope: {},
			bindToController: {
				sortByCategories: '=',
				colspan: '=?',
				items: '=',
				actions: '=?',
				defaultSortBy: '@?',
				defaultIsReverse: '@?isReverse',
				onSelect: '&?',
				selectedChangeEvent: '@?'
			},
			controllerAs: 'tableCtrl',
			controller: ['$scope', function($scope) {
				var tableCtrl = this,
					_selectedCount = 0,
					_currentEventListener = undefined,
					_categories = [];

				tableCtrl.sort = sort;
				tableCtrl.actionCheckboxChanged = actionCheckboxChanged;
				tableCtrl.allActionsCheckedChanged = allActionsCheckedChanged;
				tableCtrl.isAllItemsDisabled = false;

				var defaultSortByWatcher = $scope.$watch('tableCtrl.defaultSortBy', function(newVal) {
					tableCtrl.sortBy = tableCtrl.defaultSortBy || 'id';
					defaultSortByWatcher();
				});
				var defaultIsReverseWatcher = $scope.$watch('tableCtrl.defaultIsReverse', function(newVal) {
					tableCtrl.isReverse = tableCtrl.defaultIsReverse === 'true' || false;
					defaultIsReverseWatcher();
				});

				$scope.$watch('tableCtrl.items', _setSelectedCountAndDisabled);
				$scope.$watch('tableCtrl.selectedChangeEvent', function(newVal) {
					if (_currentEventListener) {
						_currentEventListener();
						_currentEventListener = undefined;
					}

					if (newVal) {
						_currentEventListener = $scope.$on(newVal, function() {
							_setSelectedCountAndDisabled();
							_onSelect();
						});
					}
				});
				$scope.$watch('tableCtrl.actions', function() {
					if (tableCtrl.actions && angular.isObject(tableCtrl.actions)) {
						angular.forEach(tableCtrl.actions, function(action, key) {
							$scope[key] = action;
						});
					}
				});

				$scope.$watch('tableCtrl.sortByCategories', function() {
					if(tableCtrl.sortByCategories){
						_categories = tableCtrl.items;
					}
				});
				function sort(sortBy) {
					tableCtrl.isReverse = tableCtrl.sortBy === sortBy && !tableCtrl.isReverse;
					tableCtrl.sortBy = sortBy;
					tableCtrl.label = tableCtrl.isReverse ? 'sort in ascending order':'sort in descending order';
				}

				function actionCheckboxChanged(item) {
					_setSelectedCountAndDisabled();
					_onSelect(item);
				}

				function allActionsCheckedChanged() {
					if (tableCtrl.sortByCategories) {
						angular.forEach(_categories, function (category) {
							_changeAllActions(category);
						})
					} else {
						_changeAllActions(tableCtrl.items);
					}
					_setSelectedCountAndDisabled();
					_onSelect();

				}

				function _changeAllActions(items) {
					angular.forEach(items, function (item) {
						if (item.actionsDisabled !== true) {
							item.actionsChecked = tableCtrl.allActionsChecked;
						}
					});
				}

				function _setSelectedCountAndDisabled() {
					_selectedCount = 0;
					_disabledItemsCount = 0;
					_totalItemsCount = 0;
					allItemsDisabled = [];


					if(tableCtrl.sortByCategories) {
						angular.forEach(_categories, function (category) {
							_countSelectedItems(category);
						})
					} else {
						_countSelectedItems(tableCtrl.items)
					}

					tableCtrl.isAllItemsDisabled = !!allItemsDisabled.every(function (bool) { return bool });
				}

				function _countSelectedItems(items) {
					angular.forEach(items, function(item) {
						allItemsDisabled.push(item.actionsDisabled);

						_totalItemsCount++;

						if (item.actionsChecked) {
							_selectedCount++;
						}
						if (item && item.actionsDisabled) {
							_disabledItemsCount++;
						}
					});
				}

				function _onSelect(item) {
					if (tableCtrl.onSelect) {
						tableCtrl.onSelect({ selectedCount: _selectedCount, item: item });
					}

					tableCtrl.allActionsChecked = true;

					if(tableCtrl.sortByCategories) {
						angular.forEach(_categories, function (category) {
							_checkIsAllItemsChecked(category);
						})
					} else {
						_checkIsAllItemsChecked(tableCtrl.items);
					}
				}

				function _checkIsAllItemsChecked(items) {
					var isValid = items.find(function (item) {
						return tableCtrl.isAllItemsDisabled || (!item.actionsChecked && !item.actionsDisabled);
					});

					if (!!isValid) {
						tableCtrl.allActionsChecked = false;
					}
				}
			}]
		}
	}

	/**
	 * Register service
	 */
	angular.module('spTable', [])
		.provider(name, provider)
		.directive(name, ['spTable', directive]);
})(angular);